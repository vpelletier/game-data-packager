#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2016-2018 Simon McVittie <smcv@debian.org>
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import subprocess
import sys
import typing
import unittest
from tempfile import (TemporaryDirectory)

if 'GDP_UNINSTALLED' in os.environ:
    sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
else:
    sys.path.insert(0, '/usr/share/game-data-packager')
    sys.path.insert(0, '/usr/share/games/game-data-packager')

from game_data_packager.util import (run_as_root)
from game_data_packager.version import (FORMAT, GAME_PACKAGE_VERSION)

class IntegrationTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def test_scummvm(self, install=False, method='', gain_root=''):
        with TemporaryDirectory(prefix='gdptest.') as tmp:
            os.mkdir(os.path.join(tmp, 'in'))
            os.mkdir(os.path.join(tmp, 'out'))

            K = b'\0' * 1024

            with open(os.path.join(tmp, 'in', '1K.0'), 'wb') as writer:
                writer.write(K)

            with open(os.path.join(tmp, 'in', '4K.0'), 'wb') as writer:
                for i in range(4):
                    writer.write(K)

            with open(os.path.join(tmp, 'in', '1M.0'), 'wb') as writer:
                for i in range(1024):
                    writer.write(K)

            with open(os.path.join(tmp, 'in', 'manual.pdf'), 'wb') as writer:
                writer.write(b'this is not a PDF\n')

            with open(os.path.join(tmp, 'in', 'booklet.pdf'), 'wb') as writer:
                writer.write(b'this is not a PDF either\n')

            if 'GDP_BUILDDIR' in os.environ:
                builddir = os.environ['GDP_BUILDDIR']
            else:
                builddir = os.path.join(os.getcwd(), 'out')

            if not os.path.exists(os.path.join(builddir, 'meson-info')):
                subprocess.check_call([
                    os.environ.get('MAKE', 'make'),
                    'out/tests/changelog.gz',
                    'out/tests/copyright',
                    'out/tests/vfs.zip',
                ])

            package_names = [
                'gdptest-doc',
                'gdptest6-data',
                'gdptest6-svga-data',
            ]
            data_version = '1.000.000+' + GAME_PACKAGE_VERSION
            doc_version = GAME_PACKAGE_VERSION

            env = os.environ.copy()
            env['GDP_PKGDATADIR'] = os.path.join(builddir, 'tests')

            if 'GDP_UNINSTALLED' in os.environ:
                argv = [
                    os.path.join(
                        os.environ.get('GDP_BUILDDIR', 'out'),
                        'run-gdp-uninstalled',
                    )
                ]
            else:
                argv = ['game-data-packager']

            if install:
                assert os.environ.get('GDP_TEST_DESTRUCTIVE', '')
                run_as_root(['dpkg', '--purge'] + package_names, gain_root)

                argv.append('--force-install')

                if gain_root:
                    argv.extend(['--gain-root-command', gain_root])

                if method:
                    argv.extend(['--install-method', method])
            else:
                argv.extend([
                    '-d', os.path.join(tmp, 'out'),
                ])

            argv = argv + [
                '--no-compress',
                'scummvm',
                '--no-download',
                '--no-search',
                os.path.join(tmp, 'in'),
            ]

            # stdout=2 is effectively 2>&1
            subprocess.check_call(argv, env=env, stdout=2)

            if FORMAT == 'deb' and not install:
                import debian.deb822

                doc_deb = os.path.join(
                    tmp, 'out', 'gdptest-doc_%s_all.deb' % doc_version)
                vga_deb = os.path.join(
                    tmp, 'out',
                    'gdptest6-data_%s_all.deb' % data_version)
                svga_deb = os.path.join(
                    tmp, 'out',
                    'gdptest6-svga-data_%s_all.deb' % data_version,
                )

                self.assertTrue(os.path.isfile(doc_deb))
                self.assertTrue(os.path.isfile(vga_deb))
                self.assertTrue(os.path.isfile(svga_deb))

                blob = subprocess.check_output(
                    ['dpkg-deb', '-f', doc_deb],
                )
                meta = debian.deb822.Deb822(blob)

                self.assertEqual(meta.get('Package'), 'gdptest-doc')
                self.assertEqual(meta.get('Version'), doc_version)
                self.assertEqual(meta.get('Priority'), 'optional')
                self.assertEqual(meta.get('Section'), 'local/doc')
                self.assertEqual(meta.get('Architecture'), 'all')
                self.assertEqual(meta.get('Multi-Arch'), 'foreign')
                self.assertEqual(
                    meta.get('Depends'),
                    ('gdptest1-data | gdptest2-data | gdptest3-data | '
                     'gdptest5-data | gdptest6-data | gdptest6-svga-data'),
                )
                self.assertEqual(meta.get('Recommends'), None)
                self.assertIn(
                    'Genre: Adventure',
                    meta.get('Description'))
                self.assertIn(
                    'Documentation: A ScummVM test vaguely resembling '
                    'Leisure Suit Larry 6',
                    meta.get('Description'))
                self.assertIn(
                    'Published by: nobody',
                    meta.get('Description'))

                blob = subprocess.check_output(
                    ['dpkg-deb', '-f', vga_deb],
                )
                meta = debian.deb822.Deb822(blob)

                self.assertEqual(meta.get('Package'), 'gdptest6-data')
                self.assertEqual(meta.get('Version'), data_version)
                self.assertEqual(meta.get('Priority'), 'optional')
                self.assertEqual(meta.get('Section'), 'local/games')
                self.assertEqual(meta.get('Architecture'), 'all')
                self.assertEqual(meta.get('Multi-Arch'), 'foreign')
                self.assertEqual(meta.get('Depends'), None)
                self.assertEqual(meta.get('Recommends'), 'scummvm')
                self.assertIn(
                    'Genre: Adventure',
                    meta.get('Description'))
                self.assertIn(
                    'Game: A ScummVM test vaguely resembling '
                    'Leisure Suit Larry 6',
                    meta.get('Description'))
                self.assertIn(
                    'Published by: nobody',
                    meta.get('Description'))

                blob = subprocess.check_output(
                    ['dpkg-deb', '-f', svga_deb],
                )
                meta = debian.deb822.Deb822(blob)

                self.assertEqual(meta.get('Package'), 'gdptest6-svga-data')
                self.assertEqual(meta.get('Version'), data_version)
                self.assertEqual(meta.get('Priority'), 'optional')
                self.assertEqual(meta.get('Section'), 'local/games')
                self.assertEqual(meta.get('Architecture'), 'all')
                self.assertEqual(meta.get('Multi-Arch'), 'foreign')
                self.assertEqual(meta.get('Depends'), None)
                self.assertEqual(meta.get('Recommends'), 'scummvm')
                self.assertEqual(meta.get('Breaks'), 'scummvm (<< 2.0.0~)')
                self.assertIn(
                    'Genre: Adventure',
                    meta.get('Description'))
                self.assertIn(
                    'Game: A ScummVM test vaguely resembling '
                    'Leisure Suit Larry 6',
                    meta.get('Description'))
                self.assertIn(
                    'Published by: nobody',
                    meta.get('Description'))

                contents = subprocess.check_output(
                    ['dpkg-deb', '--contents', doc_deb],
                    universal_newlines=True,
                )
                self.assertIn(
                    './usr/share/doc/gdptest-doc/\n', contents)
                self.assertIn(
                    './usr/share/doc/gdptest-doc/booklet.pdf\n', contents)
                self.assertIn(
                    './usr/share/doc/gdptest-doc/changelog.gz\n', contents)
                self.assertIn(
                    './usr/share/doc/gdptest-doc/copyright\n', contents)
                self.assertNotIn(
                    './usr/share/applications/\n', contents)

                contents = subprocess.check_output(
                    ['dpkg-deb', '--contents', vga_deb],
                    universal_newlines=True,
                )
                self.assertIn(
                    './usr/share/applications/gdptest6-data.desktop\n',
                    contents,
                )
                self.assertIn(
                    './usr/share/doc/gdptest6-data/changelog.gz\n', contents)
                self.assertIn(
                    './usr/share/doc/gdptest6-data/copyright\n', contents)
                self.assertIn(
                    './usr/share/doc/gdptest6-data/manual.pdf\n', contents)
                self.assertIn(
                    './usr/share/games/gdptest6/1K.0\n', contents)
                self.assertIn(
                    './usr/share/games/gdptest6/4K.0\n', contents)
                self.assertNotIn(
                    './usr/share/games/gdptest6/1M.0\n', contents)

                contents = subprocess.check_output(
                    ['dpkg-deb', '--contents', svga_deb],
                    universal_newlines=True,
                )
                self.assertIn(
                    './usr/share/applications/gdptest6-svga-data.desktop\n',
                    contents)
                self.assertIn(
                    './usr/share/doc/gdptest6-svga-data/changelog.gz\n',
                    contents)
                self.assertIn(
                    './usr/share/doc/gdptest6-svga-data/copyright\n', contents)
                self.assertIn(
                    './usr/share/doc/gdptest6-svga-data/manual.pdf\n', contents)
                self.assertIn(
                    './usr/share/games/gdptest6-svga/1K.0\n', contents)
                self.assertNotIn(
                    './usr/share/games/gdptest6-svga/4K.0\n', contents)
                self.assertIn(
                    './usr/share/games/gdptest6-svga/1M.0\n', contents)

            elif FORMAT == 'deb' and install:
                completed = subprocess.run(
                    [
                        'dpkg-query', '--show',
                        '--showformat', '${Package}\\t${Version}\\n',
                    ] + package_names,
                    check=False,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    universal_newlines=True,
                )

                for line in completed.stderr.splitlines(keepends=True):
                    sys.stderr.write('# ' + line)

                if completed.returncode != 0:
                    raise AssertionError(completed.stderr)

                output = completed.stdout
                assert output is not None
                installed: typing.Dict[str, str] = {}

                for line in output.splitlines():
                    name, version = line.split('\t')
                    self.assertNotIn(name, installed)
                    installed[name] = version

                self.assertIn('gdptest6-data', installed)
                self.assertEqual(installed['gdptest6-data'], data_version)
                self.assertIn('gdptest-doc', installed)
                self.assertEqual(installed['gdptest-doc'], doc_version)
                self.assertIn('gdptest6-svga-data', installed)
                self.assertEqual(installed['gdptest6-svga-data'], data_version)

                assert os.environ.get('GDP_TEST_DESTRUCTIVE', '')
                run_as_root(['dpkg', '--purge'] + package_names, gain_root)

            if 'GDP_TEST_ALL_FORMATS' in os.environ:
                for f in 'arch deb rpm'.split():
                    subprocess.check_call(
                        argv + ['--target-format', f],
                        env=env,
                    )

    def test_install_defaults(self):
        if FORMAT != 'deb':
            self.skipTest('not a .deb based OS')

        if not os.environ.get('GDP_TEST_DESTRUCTIVE', ''):
            self.skipTest('GDP_TEST_DESTRUCTIVE not set')

        self.test_scummvm(install=True)

    def test_install_sudo(self):
        if FORMAT != 'deb':
            self.skipTest('not a .deb based OS')

        if not os.environ.get('GDP_TEST_DESTRUCTIVE', ''):
            self.skipTest('GDP_TEST_DESTRUCTIVE not set')

        self.test_scummvm(install=True, gain_root='sudo', method='apt')

    def test_install_pkexec(self):
        if FORMAT != 'deb':
            self.skipTest('not a .deb based OS')

        if not os.environ.get('GDP_TEST_DESTRUCTIVE', ''):
            self.skipTest('GDP_TEST_DESTRUCTIVE not set')

        self.test_scummvm(install=True, gain_root='pkexec', method='dpkg')

    def tearDown(self):
        pass

if __name__ == '__main__':
    from gdp_test_common import main
    main()
