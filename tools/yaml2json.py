#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2015 Simon McVittie <smcv@debian.org>
# SPDX-License-Identifier: GPL-2.0-or-later

import json
import os
import sys

from game_data_packager.util import load_yaml

def main(f, out):
    data = load_yaml(open(f, encoding='utf-8'))
    with open(out + '.tmp', 'w', encoding='utf-8') as fd:
        json.dump(data, fd, sort_keys=True)
    os.rename(out + '.tmp', out)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])

