# Unreal (Classic/Gold) AppArmor profile
# Copyright © 2016-2020 Simon McVittie
# SPDX-License-Identifier: FSFAP

#include <tunables/global>

profile unreal /usr/lib/unreal-*/System/*.bin flags=(complain) {
  #include <abstractions/X>
  #include <abstractions/audio>
  #include <abstractions/base>
  #include <abstractions/dri-common>
  #include <abstractions/dri-enumerate>
  #include <abstractions/mesa>
  #include <abstractions/nameservice>
  #include <abstractions/nvidia>
  #include <abstractions/private-files-strict>

  network inet dgram,
  network inet stream,
  network inet6 dgram,
  network inet6 stream,

  /etc/libnl-*/classid r,
  /etc/machine-id r,
  /var/lib/dbus/machine-id r,
  @{PROC}/@{pid}/net/psched r,
  @{sys}/devices/system/cpu/{,**} r,

  # udev device enumeration, input devices, video
  /etc/udev/udev.conf r,
  /run/udev/data/** r,
  @{sys}/bus/ r,
  @{sys}/class/ r,
  @{sys}/class/drm/ r,
  @{sys}/class/input/ r,
  @{sys}/class/sound/ r,
  @{sys}/devices/**/drm/** r,
  @{sys}/devices/**/input/** r,
  @{sys}/devices/**/sound/**/input*/** r,
  @{sys}/devices/**/sound/**/uevent r,
  @{sys}/devices/pci*/**/config r,
  @{sys}/devices/pci*/**/revision r,

  /usr/lib/unreal-classic/System/*.bin mrix,
  /usr/lib/unreal-gold/System/*.bin mrix,

  /usr/lib/unreal/System/lib*.so* mr,
  /usr/lib/unreal-classic/System/*.so mr,
  /usr/lib/unreal-gold/System/*.so mr,

  /usr/lib/unreal-classic/** r,
  /usr/lib/unreal-gold/** r,
  /usr/share/{games/,}unreal/** r,
  /usr/share/{games/,}unreal-gold/** r,
  /usr/share/{games/,}unreal-fusion-map-pack/** r,
  /usr/share/{games/,}unreal-ut99-shared/** r,

  owner @{HOME}/.local/share/unreal{,-gold}/** rwk,
}

# vim:syntax=apparmor
